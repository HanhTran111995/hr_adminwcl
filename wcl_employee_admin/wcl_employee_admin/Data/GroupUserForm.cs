﻿namespace wcl_employee_admin.Data
{
    public class GroupUserForm
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Avatarurl { get; set; }
        public bool OnlineStatus { get; set; }



    }
}
